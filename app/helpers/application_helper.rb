module ApplicationHelper
  def flash_notice(text)
    flash[:notice] ||= []
    flash[:notice] << text
  end

  def render_notice
    flash[:notice].join("<br>")
  end
end

json.array!(@lookups) do |lookup|
  json.extract! lookup, :id, :name, :content
  json.url lookup_url(lookup, format: :json)
end

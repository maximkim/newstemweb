class RunstemController < ApplicationController
  include ApplicationHelper
  def is_i? (str)
    !!(str =~ /^[-+]?[0-9]+$/)
  end

  def is_fl? (str)
    !!(str =~ /^[-+]?[0-9]+\.[0-9]+$/)
  end

  def is_email?(str)
    !!(str =~ /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/ ) 
  end

  def check_input(errors)
    iserror = false
    errors.each do |text, cond|
      if not cond
        flash_notice text
        iserror = true
      end
    end
    iserror
  end

  def check_email
    email = false

    if is_email?(params[:email])
      email = params[:email]
    else
      flash_notice "provide email"
    end
    email
  end


  def write_files(files)
    files.each do |name, data|
      File.open(name, 'w') { |file| file.write(data) }
    end
  end
  
  
  def setconstants
    @dir = Dir.mktmpdir

    @stemfilename = "#{@dir}/test"
    @inputfile = "#{@dir}/inputfile"
    @settingsfile = "#{@dir}/settingsfile"
    @scalingfile = "#{@dir}/scalingfile"
    @assocfile = "#{@dir}/assocfile"
    @treefile = "#{@dir}/treefile"
    @genetreefile = "#{@dir}/genetreefile"
    @srcdir = APP_CONFIG["srcdir"]
    @stempy = "SpedeSTEM_2.py"
    @stemsrc = "src"
    @stemhy = "stem-hy.jar"
    @scriptlink = "#{@dir}/#{@stempy}"
    @scriptsrclink = "#{@dir}/#{@stemsrc}"
    @stemhylink = "#{@dir}/#{@stemhy}"
    @python = APP_CONFIG["python"]
    File.symlink("#{@srcdir}/#{@stempy}", @scriptlink)
    File.symlink("#{@srcdir}/#{@stemsrc}", @scriptsrclink)
    File.symlink("#{@srcdir}/#{@stemhy}", @stemhylink)

  end

  
  def cleanup
    # @lookup = Lookup.where(name: "cleanup").first
    File.unlink(@scriptlink)
    File.unlink(@scriptsrclink)
    File.unlink(@stemhylink)
    FileUtils.remove_entry @dir
  end
  
  def zip_files (filenames) 
    require 'rubygems'
    require 'zip'
    stringio = Zip::OutputStream.write_buffer do |zio|

      filenames.each do |filename|
        if File.exist? "#{@dir}/#{filename}"
          zio.put_next_entry("#{filename}")
          File.open("#{@dir}/#{filename}", 'r') { |file| zio.write(file.read)}
        else
          flash_notice "#{filename} not found"
        end
      end

    end

    stringio.rewind
    stringio.sysread   
  end

  def report_zip(email, binary_data)
    RunstemMailer.result_email(email, binary_data).deliver
    flash_notice "email has been sent to #{email}"
  end


  def handle_lookup(name)
      @lookup = Lookup.where(name: name).first
      if @lookup
          @content = @lookup.content
      else
          @content = "<h3>"+ name + "</h3>"
      end
  end  

  def index
      handle_lookup("runspedestem")
  end

  def result

  end

  def validationform
      handle_lookup "validation"
  end

  def validation
    errors = Hash.new("unknown error")
    errors["no settings file"] = params[:settingsfile]
    errors["no gene tree file"] = params[:genetreefile]
    errors["no association file"] = params[:assocfile]

    iserror = check_input(errors)

    email = check_email()
    if not email
      iserror = true
    end

    if iserror
      redirect_to validationform_path and return
    end

    setconstants

    files = Hash.new
    files[@settingsfile] = params[:settingsfile].read
    files[@assocfile] = params[:assocfile].read
    files[@genetreefile] = params[:genetreefile].read

    write_files(files)

    begin
      # use the directory...
      # command > file-name 2>&1
      commandstr = "cd #{@dir} && #{@python} #{@stempy} validation -s #{@settingsfile}  -t #{@genetreefile} -a #{@assocfile} -q > output 2>&1" 

      # Completing Analysis... See 'results.txt' and 'itTable.txt' files

      system(commandstr)

      binary_data = zip_files(["output", "itTable.txt", "results.txt"])
      report_zip(email, binary_data)
      redirect_to validationform_path

    ensure
      # remove the directory.
      cleanup
    end
  end
  
  def discoveryform
      handle_lookup "discovery"
  end

  def discovery
    withsettings = false

    errors = Hash.new("unknown error")
    errors["no settings file"] = params[:settingsfile]
    errors["no gene tree file"] = params[:genetreefile]

    iserror = check_input(errors)

    email = check_email()

    if not email
      iserror = true
    end

    if iserror
      redirect_to discoveryform_path and return
    end
    
    setconstants


    files = Hash.new
    files[@settingsfile] = params[:settingsfile].read
    files[@genetreefile] = params[:genetreefile].read

    write_files(files)
    
    begin
      # command > file-name 2>&1
      commandstr = "cd #{@dir} && #{@python} #{@stempy} discovery -s #{@settingsfile}  -t #{@genetreefile} -q > output 2>&1" 

      # Completing Analysis... See 'results.txt' and 'itTable.txt' files

      system(commandstr)

      binary_data  = zip_files(["output", "results.txt", "itTable.txt"])

      #send_data(binary_data, :filename => "out.zip", :type => "application/zip")

      report_zip(email, binary_data)
      redirect_to discoveryform_path

    ensure
      # remove the directory and unlink
      cleanup
    end
    # render action: 'result'
  end
  

  def subsamplingform
      handle_lookup "subsampling"
  end

  def subsampling
    withsettings = false
    iserror = false


    
    if not params[:inputfile] 
      flash_notice "no nexus file"
      iserror = true
    end
    
    if not params[:assocfile]
      flash_notice "no traits file"
      iserror = true
    end


    logger.debug("inputfiletype| #{params[:inputfile].content_type}")
    logger.debug("assocfiletype| #{params[:assocfile].content_type}")
    
    # if not params[:inputfile].content_type.start_with?("text/") 
    #   flash_notice "bad nexus file"
    #   iserror = true
    # end
    
    # if not params[:assocfile].content_type.start_with?("text/") 
    #   flash_notice "bad traits file"
    #   iserror = true
    # end
    
    np = params[:np]
    if not is_i?(np) && np.to_i < 100
      flash_notice "incorrect number per population"
      iserror = true
    end
    
    rp = params[:rp]
    if not (is_i?(rp) && rp.to_i < 100)
      flash_notice "incorrect number of replicates"
      iserror = true
    end

    email = check_email()
    if not email
      iserror = true
    end
    
    if iserror
      redirect_to subsamplingform_path and return
    end
    
    setconstants
    
    stemdata = params[:inputfile].read
    File.open(@inputfile, 'w') { |file| file.write(stemdata) }

    stemdata = params[:assocfile].read
    File.open(@assocfile, 'w') { |file| file.write(stemdata) }

    stemdata = params[:settingsfile]
    if stemdata
      File.open(@settingsfile, 'w') { |file| file.write(stemdata) }
      withsettings = true
    end

      
    begin
      # use the directory...
      commandstr = "cd #{@dir} && #{@python} #{@stempy} subsampling -i #{@inputfile}  -a #{@assocfile} "
      if withsettings
        commandstr = commandstr + " -ap #{@settingsfile} "
      end
      
      commandstr = commandstr + " -o new.nexus -np #{np} -rp #{rp} > output 2>&1"
      system(commandstr)

      binary_data = zip_files(["output", "new.nexus"])
      report_zip(email, binary_data)
      redirect_to subsamplingform_path

    ensure
      # remove the directory.
      cleanup
    end
    # render action: 'result'
  end
  
  def generatesettingsform
      handle_lookup "generatesettings"
  end
  
  def generatesettings

    errors = Hash.new("unknown error")
    errors["no traits file"] = params[:traitsfile]
    errors["BT value is incorrect"] = is_fl?(params[:bt])

    iserror = check_input(errors)


    email = check_email()
    if not email
      iserror = true
    end

    if iserror 
      redirect_to generatesettingsform_path and return
    end

    bt = params[:bt]
    setconstants

    stemdata = params[:traitsfile].read
    File.open(@treefile, 'w') { |file| file.write(stemdata) }

    begin
      # use the directory...

      system("cd #{@dir} && #{@python} #{@stempy} setup -t #{@treefile} -bt #{bt} > output 2>&1")

      binary_data = zip_files(["output", "settings.stem"])
      report_zip(email, binary_data)
      redirect_to generatesettingsform_path
    ensure
      # remove the directory.
      cleanup
    end
  end

  def cleantreesform
      handle_lookup "cleantrees"
  end

  def cleantrees
    iserror = false
    treefiles = params[:treefiles]
    errors = Hash.new

    if not treefiles
      flash_notice "no tree files given"
      iserror = true
      treefiles = []
    end

    treefiles.each do |treefile|
      if not treefile and not iserror
        iserror = true
        flash_notice "tree files missing"
      end
    end

    if not params[:scalingfile]
      iserror = true
      flash_notice "scaling file missing"
    end
    
    email = check_email()
    if not email
      iserror = true
    end

    if iserror 
      redirect_to cleantreesform_path and return
    end

    setconstants

    treecount = treefiles.size
    # logger.debug("treecount #{treecount}" )

    writehash = Hash.new
    treefiles.each_with_index do |treefile, index|
      writehash["#{@treefile}#{index}"] = treefile.read
    end
    
    writehash[@scalingfile] = params[:scalingfile].read

    write_files(writehash)

    begin
      # use the directory...
      sysstr = "cd #{@dir} && #{@python} #{@stempy} setup -c "

      (0 .. treecount-1).each do |index|
        sysstr += " treefile#{index} "
      end

      sysstr += " -s #{@scalingfile}  > output 2>&1"

      system(sysstr) 
      # logger.debug("sysstr : " + sysstr)
      
      concat = File.open("#{@dir}/concatclean", 'w')
      Dir["#{@dir}/cleaned*"].each do |fname|
        File.open(fname, 'r') { |file| concat.write(file.read) }
      end
      concat.close
      
      binary_data = zip_files(["output", "concatclean"])
      report_zip(email, binary_data)
      redirect_to cleantreesform_path

    ensure
      # remove the directory.
      cleanup
    end
    
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def runstem_params
      params.require(:lookup).permit(:name, :content)
    end
end

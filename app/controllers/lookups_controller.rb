class LookupsController < ApplicationController
  before_action :set_lookup, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:edit, :update, :destroy, :new, :create]
    
  include ApplicationHelper
  def main 
      handle_lookup("main")
  end

  def contact 
      handle_lookup("contact")
  end

  def handle_lookup(name)
      @lookup = Lookup.where(name: name).first
      if @lookup
          @content = @lookup.content
      else
          @content = "Nothing here yet."
      end
      respond_to do |format|
          format.html { render name}
          format.json { render text: @content } 
      end
  end

  def mailhelp (dest_email)
    # carstens_email = APP_CONFIG["carstens_email"]
    useremail = params[:from]
    subject = params[:subject]
    body = params[:body]
    RunstemMailer.help_email(useremail, dest_email, subject, body).deliver
    flash_notice "email has been sent from #{useremail}"
    redirect_to contact_path
  end

  def genhelp
      mailhelp APP_CONFIG["carstens_email"]
  end

  def techhelp
      mailhelp APP_CONFIG["kim_email"]
  end

  # GET /lookups
  # GET /lookups.json
  def index
    @lookups = Lookup.all
  end

  # GET /lookups/1
  # GET /lookups/1.json
  def show
  end

  # GET /lookups/new
  def new
    @lookup = Lookup.new
  end

  # GET /lookups/1/edit
  def edit
      session[:return_to] = request.referer
  end


  # POST /lookups
  # POST /lookups.json
  def create
    @lookup = Lookup.new(lookup_params)
    respond_to do |format|
      if @lookup.save
        format.html { redirect_to @lookup, notice: 'Lookup was successfully created.' }
        format.json { render action: 'show', status: :created, location: @lookup }
      else
        format.html { render action: 'new' }
        format.json { render json: @lookup.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lookups/1
  # PATCH/PUT /lookups/1.json
  def update
    respond_to do |format|
      if @lookup.update(lookup_params)
        format.html { redirect_to session.delete(:return_to), notice: 'Lookup was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @lookup.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lookups/1
  # DELETE /lookups/1.json
  def destroy
    @lookup.destroy
    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lookup
      @lookup = Lookup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lookup_params
      params.require(:lookup).permit(:name, :content)
    end
end

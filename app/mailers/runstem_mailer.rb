class RunstemMailer < ActionMailer::Base
  default from: "no-reply@spedestem.asc.ohio-state.edu"

  def welcome_email(user)
    @user = user
    # @url  = "http://example.com/login"
    mail(:to => user.email, :subject => "Welcome to My Awesome Site")
  end

  def result_email(email, zipfile)
    # @user = user
    attachments['out.zip'] = {:mime_type => 'application/zip',
      :content => zipfile}

    mail(:to => email, :subject => "SpedeStem results")
  end

  def help_email(user_email, toemail, subject, body)
    mail(:from => user_email, :subject => subject, :to => toemail, :body => body)
  end
end

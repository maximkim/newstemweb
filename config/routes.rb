Newstemweb::Application.routes.draw do

  resources :posts

  devise_for :admins
  mount Ckeditor::Engine => '/ckeditor'
  resources :lookups

  root to: "lookups#main"
  match "runspedestem" => "runstem#index", :as => "runstem", :via => "get"
  match "runspedestem/subsampling" => "runstem#subsamplingform", :as => "subsamplingform", :via => "get"
  match "runspedestem/subsampling" => "runstem#subsampling", :as => "subsampling", :via => "post"
  match "runspedestem/generatesettings" => "runstem#generatesettingsform", :as => "generatesettingsform", :via => "get"
  match "runspedestem/generatesettings" => "runstem#generatesettings", :as => "generatesettings", :via => "post"
  match "runspedestem/discovery" => "runstem#discoveryform", :as => "discoveryform", :via => "get"
  match "runspedestem/discovery" => "runstem#discovery", :as => "discovery", :via => "post"

  match "runspedestem/validation" => "runstem#validationform", :as => "validationform", :via => "get"
  match "runspedestem/validation" => "runstem#validation", :as => "validation", :via => "post"

  
  match "runspedestem/cleantrees" => "runstem#cleantreesform", :as => "cleantreesform", :via => "get"
  match "runspedestem/cleantrees" => "runstem#cleantrees", :as => "cleantrees", :via => "post"


  match "/contact" => "lookups#contact", :as => "contact", :via => "get"


  match "contact/genhelp" => "lookups#genhelpform", :as => "genhelpform_contact", :via => "get"
  match "contact/genhelp" => "lookups#genhelp", :as => "genhelp_contact", :via => "post"

  match "contact/techhelp" => "lookups#techhelpform", :as => "techhelpform_contact", :via => "get"
  match "contact/techhelp" => "lookups#techhelp", :as => "techhelp_contact", :via => "post"


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

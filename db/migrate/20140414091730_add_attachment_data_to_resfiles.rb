class AddAttachmentDataToResfiles < ActiveRecord::Migration
  def self.up
    change_table :resfiles do |t|
      t.attachment :data
    end
  end

  def self.down
    drop_attached_file :resfiles, :data
  end
end

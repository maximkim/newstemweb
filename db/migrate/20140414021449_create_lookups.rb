class CreateLookups < ActiveRecord::Migration
  def change
    create_table :lookups do |t|
      t.string :name
      t.text :content

      t.timestamps
    end
  end
end
